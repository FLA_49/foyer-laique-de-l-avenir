---
title: "Histoire du Foyer Laïque de l'Avenir"
order: 10
in_menu: false
---
Le 21 mai 1905, Mr Ernest Moriceau, instituteur, fonde l’association des « Anciens Élèves et Amis de l’École Laïque de Doué-la-Fontaine ». Elle a pour but « d’entretenir et de resserrer les relations d’amitié qui se sont formées à l’école, de faciliter l’enseignement donné aux adultes, de rechercher des places d’employés ou d’apprentis au sortir de l’école... ». En 1907, l’association s’installe 10 rue de Taunay en compagnie de la Société Philharmonique et du Comité Républicain. Ces trois associations fusionneront plus tard en une seule : l’Avenir. La société de jeunes filles «Printania» les rejoindra par la suite. En 1924, l’Avenir devient un « Foyer d’éducation physique, intellectuelle et morale » et en 1965, un « Foyer laïque de Jeunes et d’Éducation Populaire ».

Tout au long de ses 110 ans d’existence, l’Avenir proposa à ses sociétaires de nombreuses activités culturelles et sportives : tir, musique, bibliothèque, organisation de fêtes locales, défilés fleuris, théâtre, escrime, football, athlétisme, rotins, émaux, marionnettes, cinéma, photo, vidéo... En parallèle, l’Avenir propose aussi des actions en direction des scolaires.

Au début des années 1960, l’Avenir proposa également des séances de cinéma récréatif ou éducatif. Elles perdureront jusqu’en 1981. Attachés à leur passé cinématographique, les bénévoles de l’Avenir s’investiront dès 1984 avec l’ABC49 pour proposer aux douessins des films récents. Ce partenariat durera 30 ans.

Aujourd’hui, le Foyer propose toujours de nombreuses activités : billard, gymnastique, tennis de table, ateliers linguistiques, photo et peinture, scrabble...et le cinéma tous les mercredis soir en partenariat avec Ballad'Images de familles rurales et la Communauté de communes de la Région de Doué la Fontaine. 