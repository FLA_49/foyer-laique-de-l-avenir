Bonjour [% user.gecos %],

Ce mois-ci, retrouvez quatre films à l'affiche du cinéma de Doué-en-Anjou!

![](https://gitlab.com/FLA_49/foyer-laique-de-l-avenir/-/raw/main/images/20240605_leMalExistePas.jpg?ref_type=heads)
![](https://gitlab.com/FLA_49/foyer-laique-de-l-avenir/-/raw/main/images/20240612_freres.jpg?ref_type=heads)
![](https://gitlab.com/FLA_49/foyer-laique-de-l-avenir/-/raw/main/images/20240619_petiteVadrouille.jpg?ref_type=heads)
![](https://gitlab.com/FLA_49/foyer-laique-de-l-avenir/-/raw/main/images/20240626_4amesCoyote.jpg?ref_type=heads)

---

# [LE MAL N'EXISTE PAS](https://keskonfai.fr/events/80f92ca5-7e52-4b96-a14d-92438d6c53c1)
Mercredi 5 juin, 20h30

*Avec Allociné*

    10 avril 2024 en salle | 1h 46min | Drame
    De Ryūsuke Hamaguchi | Par Ryūsuke Hamaguchi
    Avec Hitoshi Omika, Ryo Nishikawa, Ryûji Kosaka
    Titre original: *Aku wa sonzai shinai*

*Tout public*

Takumi et sa fille Hana vivent dans le village de Mizubiki, près de Tokyo. Comme leurs aînés avant eux, ils mènent une vie modeste en harmonie avec leur environnement. Le projet de construction d’un « camping glamour » dans le parc naturel voisin, offrant aux citadins une échappatoire tout confort vers la nature, va mettre en danger l’équilibre écologique du site et affecter profondément la vie de Takumi et des villageois...

[*Je cherche ou je propose un covoiturage pour cette séance*](https://covievent.org/covoiturage/seances-de-cinema-a-doue-en-anjou/c5a2280243a695c05fd7736a7c232263/2024-06-05)

---

# [FRÈRES](https://keskonfai.fr/events/bde0f18e-22cb-43d1-8cec-26b10f1cac12)

Mercredi 12 juin, 20h30

*Avec Allociné*

    24 avril 2024 en salle | 1h 46min | Drame
    De Olivier Casas
    Avec Mathieu Kassovitz, Yvan Attal, Alma Jodorowsky

*Tout public*

L’histoire vraie de deux petits garçons de 5 et 7 ans qui, abandonnés par leur mère en 1948, s'enfuient dans la forêt. Ils vont y survivre pendant sept années et tisser un lien qui les unira à jamais. Des décennies plus tard, les deux frères quittent tout pour se retrouver. Mais le passé et les secrets les rattrapent, même à l'autre bout du monde.

[*Je cherche ou je propose un covoiturage pour cette séance*](https://covievent.org/covoiturage/seances-de-cinema-a-doue-en-anjou/c5a2280243a695c05fd7736a7c232263/2024-06-12)

---

# [LA PETITE VADROUILLE](https://keskonfai.fr/events/478c52e6-eaf2-4b5b-8beb-1d9049d9ec66)
Mercredi 19 juin, 20h30

*Avec Allociné*

    5 juin 2024 en salle | 1h 36min | Comédie
    De Bruno Podalydès | Par Bruno Podalydès
    Avec Daniel Auteuil, Sandrine Kiberlain, Denis Podalydès

*Tout public*

Justine, son mari et toute leur bande d'amis trouvent une solution pour résoudre leurs problèmes d'argent : organiser une fausse croisière romantique pour Franck, un gros investisseur, qui cherche à séduire une femme.

[*Je cherche ou je propose un covoiturage pour cette séance*](https://covievent.org/covoiturage/seances-de-cinema-a-doue-en-anjou/c5a2280243a695c05fd7736a7c232263/2024-06-19)

---

# [Les 4 ÂMES DU COYOTE](https://keskonfai.fr/events/10ba8247-d049-4f3a-9a38-0117ad1b017c)
Mercredi 26 juin, 20h30

*Avec Allociné*

    15 mai 2024 en salle | 1h 43min | Aventure, Animation
    De Áron Gauder
    Par Áron Gauder, Géza Bereményi
    Avec Lorne Cardinal, Danny Kramer, Diontae Black
    Titre original Kojot négy lelke

*Tout public*

Des activistes amérindiens s'opposent à un projet d'oléoduc sur leur territoire ancestral. Le soir autour du feu, ils se réunissent autour de leur Grand-Père qui leur fait le récit de la Création. Le conte rappelle à tous la place de l’Homme sur la Terre et son rôle dans la destruction de la nature.

[*Je cherche ou je propose un covoiturage pour cette séance*](https://covievent.org/covoiturage/seances-de-cinema-a-doue-en-anjou/c5a2280243a695c05fd7736a7c232263/2024-06-26)

---

<center>
Vous souhaitez contribuer à la diffusion de la programmation du cinéma de Doué-en-Anjou?

[Télécharger l'affiche du mois de juin](https://conextcloud.communecter.org/s/DaxraR8GFmRmrWS)

[Invitez vos amis à s'inscrire à la liste de diffusion en partageant ce lien](https://framagroupes.org/sympa/subscribe/cinema_doueenanjou?previous_action=info)

**L'équipe du cinéma vous remercie chaleureusement.**
</center>

---


C'est ainsi que l'année s'achève. L'équipe du cinéma prend des vacances cet été, et sera heureuse de vous accueillir de nouveau au mois de septembre.
Nous vous souhaitons de belles vacances !

**_La section cinéma du Foyer Laïque de l'Avenir_**


