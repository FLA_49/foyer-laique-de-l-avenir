---
title: "Langues et languages"
order: 7
in_menu: false
---
<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;starting_hour=8&amp;tab=week&amp;tabs=month&amp;tabs=week&amp;tabs=agenda&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FMiZeHi7eXxb7GZzo%3Fexport"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="600px" width="100%"></iframe>

## Anglais
Séance le mardi, de 18h15 à 19h15

**Contact:**
- 02 41 59 07 14
- 06 40 09 66 19

## Dictée plaisir
Séance chaque troisième vendredi du mois, de 14h à 16h.

**Contact:**
- 06 40 09 66 19

## Espagnol
Séance le jeudi, de 10h à 11h

**Contact:**
- 02 41 59 07 14
- 06 40 09 66 19 

## Scrabble
Séance le lundi de 14h30 à 17h.

**Contact:**
- 02 41 59 07 42 