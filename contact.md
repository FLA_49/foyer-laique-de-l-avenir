---
title: "Contact"
order: 1
in_menu: true
---
Voici divers moyens de contacter le Foyer Laïque : 
+ 📧 <a href="mailto:fla_doue@tutamail.com?subject=Demande d'information">fla_doue@tutamail.com</a>
+ ☎️ 02 41 59 14 46

### Où nous trouver?
L'adresse du Foyer Laïque de l'Avenir est :

- 10 rue de Taunay </br> 49700 Doué-en-Anjou

<iframe width="100%" height="500" src="https://www.openstreetmap.org/export/embed.html?bbox=-0.3118228912353516%2C47.17970823181453%2C-0.24418830871582034%2C47.205896077911575&amp;layer=mapnik&amp;marker=47.192803770194956%2C-0.27800559997558594" style="border: 1px solid black"></iframe> 

### Et pour recevoir nos actualités

<iframe width="100%" height="900" src="https://de28ef0b.sibforms.com/serve/MUIFAJyfdcsnapkG8Xj4MdL3K_XJqlWsQIsHZ6XNXRR4YCTMAYcGqN-z9X6w9meXoqmZbiCQ1C6Brzv3APVFNQfqr52mqZfRX2t_BWkJgGqyDhkUf03RM2b_QkihQ4iqJCjoks_MGPfRqTrebHaQh87hnr_bSzRHbiWlZtAFui1OSRXnw0qDp7QP7UiQWHdW0C9F3NfMGAbHzNwT" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>

### Histoire du Foyer

[Retrouvez ici l'histoire centenaire de l'association](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/histoire%20du%20foyer%20laique%20de%20l'avenir.html) 