---
title: "Activités artistiques"
order: 3
in_menu: false
---
<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;starting_hour=8&amp;tab=week&amp;tabs=month&amp;tabs=week&amp;tabs=agenda&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FX4fPRNiw2koCpY3e%3Fexport"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="800px" width="100%"></iframe>

## Atelier photo numérique

*Cet atelier réunit des passionnés de la photo.*

*Vous apprendrez à maîtriser votre appareil photo numérique et à l'utiliser avec plaisir dans toutes ses possibilités et à réaliser des court-métrages photos pour donner vie à vos photos, dans une ambiance très conviviale.*

*Notre atelier Ombres et Lumières créé en 2009, participe régulièrement à des manifestations locales comme le Téléthon et les Journées de la Rose. Il a réalisé des court-métrages photos à destination de la filière des rosiéristes, et chaque année un court-métrage photos est produit pour les journées de la Rose.*

*Un court-métrage est projeté tous les jours dans le site " La cave vivante du champignon " au Puy Notre Dame.*

*Cet atelier est animé pas des passionnés de la photographie et de l'audiovisuel depuis plusieurs décennies.*

*Séances de 20h à 22h deux fois par mois.*

Séance le jeudi, de 20h à 22h

**Contact:**
- 06 77 34 12 27

## Peinture aquarelle

*La section Peinture/Aquarelle de l’association est une des nombreuses activités que propose Le Foyer Laïque L’Avenir. Elle a été ouverte en Avril 2011 et pratique l’aquarelle depuis septembre 2012.*


*Cet Atelier, animé par une artiste aquarelliste, Michèle Grimaud, accueille à l’année, un groupe de 12 à 14 personnes. Divers sujets y sont abordés selon les centres d’intérêt, les saisons, les coups de cœur…*

*La technique de l’aquarelle y est appréhendée sous différents aspects ( travail dans le mouillé, à sec,…) et toujours dans une grande convivialité !*

Séance le mardi de 14h à 16h30

**Contact:**
- 06 81 54 43 88
- 06 40 09 66 19 