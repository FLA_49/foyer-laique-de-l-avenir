---
title: "Sport et bien-être"
order: 8
in_menu: false
---
<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;starting_hour=8&amp;tab=week&amp;tabs=month&amp;tabs=week&amp;tabs=agenda&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FsrPPfmyMytpSDGkX%3Fexport"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="800px" width="100%"></iframe>

## Billard français
A partir de 17h30.

**Contact:**
- 06 11 54 23 28

## Gymnastique fitness

2 séances chaque jeudi soir :
- 18h à 19h15
- 19h15 à 20h15

**Contact:**
- 06 40 09 66 19
- 06 72 68 62 11 

## Qi-gong

*Le qi gong, chi gong ou chi kung (chinois simplifié : 气功 ; chinois traditionnel : 氣功 ; pinyin : qìgōng ; Wade : ch'i gong), est une gymnastique traditionnelle chinoise et une science de la respiration, fondée sur la connaissance et la maîtrise de l'énergie vitale, et associant mouvements lents, exercices respiratoires et concentration. Le terme signifie littéralement "exercice (gong) relatif au qi", ou "maîtrise de l'énergie vitale".*

Séance le mardi de 10h15 à 11h30.

**Contact:**
- 06 83 18 51 97 

## Sophrologie

Séance un lundi sur deux (en semaine paire), de 11h à 12h.

**Contact:**
- 06 63 47 54 18
- 06 72 68 62 11

## Taï chi

*Le tai-chi est une gymnastique énergétique globale qui consiste à réaliser un ensemble de mouvements continus et circulaires exécutés avec lenteur et précision dans un ordre préétabli.*

*Cette gymnastique énergétique globale se décline en plusieurs styles : certaines écoles visent surtout la prise de conscience de soi par une approche intérieure, tandis que d’autres favorisent les techniques de combat. La plupart des écoles ont toutefois abandonné leur intention martiale au profit du développement de la souplesse et de l'éveil du Qi.*

Séance le lundi, de 19h à 20h30

**Contact:**
- 07 81 88 81 89 

## Yoga

*Le Yoga est une discipline du corps et de l’esprit qui comprend une grande variété d’exercices et de techniques.*

*Les techniques employées utilisent des postures physiques (appelées asanas), des pratiques respiratoires (pranayama) et de méditation, ainsi que la relaxation profonde (yoga nidra).*

Séance le mardi, de 18h15 à 19h30

**Contact:**
- 06 50 58 76 62
- 06 72 68 62 11 

## Yoga du rire

Séance un lundi sur deux, de 19h à 20h 