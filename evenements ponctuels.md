---
title: "Evénements ponctuels"
order: 6
in_menu: true
---
<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;starting_hour=8&amp;tab=agenda&amp;tabs=month&amp;tabs=week&amp;tabs=agenda&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FoR8BwHwN3rpc6fpJ%3Fexport"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="600px" width="100%"></iframe>

## Concours de belote

Les prochains concours de belote auront lieu les samedi 23 novembre 2024 et samedi 15 janvier 2025. Les inscriptions ouvrent à partir de 13h30.

Les lots habituels sont des jambons, langues de bœuf et cochon détaillé.

N'oubliez pas de venir avec votre binôme!

## Réveillon de nouvel an

Comme chaque année, le Foyer Laïque organise un réveillon de la Saint Sylvestre.

La soirée sera animée par les frères Pierjean. L'un est musicien, l'autre chanteur, et ils n'ont qu'une seule mission : vous accompagner pour une agréable Saint-Sylvestre!

Pour découvrir le menu de la soirée et vous inscrire, veuillez télécharger et imprimer [ce bulletin d'inscription](https://conextcloud.communecter.org/s/pCHjFWGK5qyNnSe).

## Concours de pêche

Plus d'informations à venir prochainement! 