---
title: "Cinéma"
order: 5
in_menu: true
---
[Suivez toute notre actualité sur notre compte Mastodon!](https://mastoot.fr/@cinema_de_doueenanjou)

### Programmation

La Ville de **Doué-en-Anjou**, **Familles Rurales et Balad'Images**, et le **Foyer Laïque de l'Avenir** sont heureux de vous présenter les séances de cinéma au théâtre Philippe Noiret.

Retrouvez le détail de toute la programmation du cinéma sur [Keskonfai.fr](https://keskonfai.fr/@cinema_de_doueenanjou).

<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;tab=agenda&amp;tabs=month&amp;tabs=agenda&amp;url=https%3A%2F%2Fkeskonfai.fr%2F%40cinema_de_doueenanjou%2Ffeed%2Fics"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="600px" width="100%"></iframe> 

Les séances sont possibles grâce aux bénévoles à la billetterie et en régie.

Si vous souhaitez participer à l'animation du territoire, retrouvez-nous à la fin d'une séance de cinéma!

### Venir au cinéma

Vous venez au cinéma et vous avez de la place dans la voiture? </br>
Vous voulez venir voir le film, mais n'avez pas de moyen de locomotion? </br>
Pourquoi ne pas covoiturer avec vos voisins pour venir au cinéma?

Alors n'hésitez pas à signalez votre besoin ou votre proposition!

Attention, toute personne qui s'engage pour un trajet aller assure aussi un retour!

### Tarifs

- Règlements acceptés : uniquement espèces et chèques
- Billetterie sur place 30 minutes avant le début de la séance, pas de réservation
- Tarif publique adultes : 6 €  enfants - de 14 ans 5 €
- Tarif adhérents Familles rurales : adultes 5 €  enfants - de 14 ans 4 € 

### Se tenir informé de l'actualité du cinéma

Nous avons mis en place une info-lettre mensuel pour diffuser la programmation du cinéma :

<iframe width="540" height="1080" src="https://de28ef0b.sibforms.com/serve/MUIFACjtioVq5Pyd8B58zuv7fzFDT_YZFqYj1k0DKAn5GJKx2OgHxauopweM0no3r4wyBzL9l4tNxt6kvyFl7a1YoxcJYx99qDoJTc-vLQhdztfYQ_c3CQQVoqVubuUKZg1-_awXbz3dm8EpRRVTcjzlHUfvxEzjaV3h-q66_St9cVqHdunX_uJlQgEayonWql6vQj9EHvroWiLK" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe> 

### Historique des info-lettres

+ [septembre 2024](https://e9muo.r.ag.d.sendibm3.com/mk/mr/sh/OycXxko2a8zXNsWLMD2eGyHg/_GEZc_0DCxCf) 