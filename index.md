---
title: "Accueil"
order: 0
in_menu: true
---
![logo FLA]({% link images/logo_FLA.png %})


# Activités du Foyer Laïque de l'Avenir

Depuis sa création en 1905, le Foyer laïque de jeunes et d'éducation populaire l'Avenir participe activement à la vie de la cité. Plusieurs activités vous sont proposées. N'hésitez pas à contacter les responsables de celles-ci pour tous renseignements.

- [**Activités artistiques**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/activites%20artistiques.html): Atelier photos numériques et Peinture aquarelle
- [**Bar associatif**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/bar%20associatif.html)
- [**Cinéma**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/cinema.html)
- [**Concours et réveillon**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/evenements%20ponctuels.html): Concours de belote, concours de pêche et réveillon du nouvel an
- [**Langues et langages**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/langues%20et%20languages.html): Anglais, Dictée plaisir, Espagnol, Scrabble
- [**Sport et bien-être**](https://foyer-laique-de-l-avenir-fla-49-365fe65d0c6ff16b11299aab8607728.gitlab.io/sport%20et%20bien-etre.html): Billard français, Gymnastique fitness, Qi gong, Sophrologie, Taï chi, Yoga, Yoga du rire 

# Agenda de l'association
<iframe id="open-web-calendar" 
    style="background:url('https://raw.githubusercontent.com/niccokunzmann/open-web-calendar/master/static/img/loaders/circular-loader.gif') center center no-repeat;"
    src="https://open-web-calendar.hosted.quelltext.eu/calendar.html?language=fr&amp;starting_hour=8&amp;tab=week&amp;tabs=month&amp;tabs=week&amp;tabs=day&amp;tabs=agenda&amp;url=https%3A%2F%2Fkeskonfai.fr%2F%40cinema_de_doueenanjou%2Ffeed%2Fics&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FX4fPRNiw2koCpY3e%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FMiZeHi7eXxb7GZzo%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FmRKQHn435DMBCCkY%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FsrPPfmyMytpSDGkX%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FoR8BwHwN3rpc6fpJ%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FekEBPJZfdRCxYxso%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FSBNkcSPC5gp5WzpE%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FW3GdArs5ZAoyyBH2%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FniQGTm5ws7PCXaag%3Fexport&amp;url=https%3A%2F%2Fframagenda.org%2Fremote.php%2Fdav%2Fpublic-calendars%2FpCZR5pyZ2MS7teiq%3Fexport"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="800px" width="100%"></iframe> 